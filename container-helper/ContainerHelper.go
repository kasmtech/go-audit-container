package ContainerHelper

import (
	"fmt"
	ps "github.com/mitchellh/go-ps"
	"io/ioutil"
	"regexp"
	"strings"
	"os"
	"log"
)

var l = log.New(os.Stdout, "", 0)
var re = regexp.MustCompile("@/containerd-shim/moby/([a-f0-9]{64})")
var debug = isEnvExist("GO_AUDIT_DEBUG")

type ContainerUtil struct {
	pidCache PidCache
}

func isEnvExist(key string) bool {
	_, exists := os.LookupEnv(key)
	return exists
}

func DebugMessage(message string) {
	if debug {
		l.Println(message)
	}
}

// NewContainer instantiates a default password store
func NewContainerUtil() ContainerUtil {
	return ContainerUtil{
		NewPidCache(),
	}
}

func (cu ContainerUtil) Init() error {
	return cu.pidCache.Init()
}

func (cu ContainerUtil) GetContainerId(pid int) (string, error) {

	cid, err := cu.pidCache.Get(pid)

	if err == nil {
		return cid, nil
	}

	p, err := ps.FindProcess(pid)

	if err != nil || p == nil {
		DebugMessage(fmt.Sprintf("Error: process not found: %d", pid))
		return "", err
	}

	not_init := true
	var ncid int = 0 //current pid, starting with the one passed in
	var uuid, unix_sock_file string = "", "" 
	for not_init {
		ncid = p.Pid()
		DebugMessage(fmt.Sprintf("Info: Process %s, %d", p.Executable(), ncid))
		if strings.HasPrefix(p.Executable(), "containerd-shim") || strings.HasPrefix(p.Executable(), "docker-containe") {
			DebugMessage(fmt.Sprintf("Found container shim for %d", pid))

			//find the container uuid from the container shim
			unix_sock_file = fmt.Sprintf("/proc/%d/net/unix", ncid)

			b, err := ioutil.ReadFile(unix_sock_file)
			if err == nil {
				rs := re.FindStringSubmatch(string(b))
				if rs != nil {
					uuid = rs[1]
					DebugMessage(fmt.Sprintf("Info: UUID %s for %d", uuid, pid))
					cu.pidCache.Set(pid, uuid)
					return uuid, nil
				} else {
					DebugMessage(fmt.Sprintf("Error: file /proc/%d/net/unix did not match regex. (%s)", ncid, string(b)))
				}
			} else {
				DebugMessage(fmt.Sprintf("Error: Exception accessing file /proc/%d/net/unix: %s", ncid, err))
			}
		}

		p, err = ps.FindProcess(p.PPid())

		if p == nil || err != nil {
			DebugMessage(fmt.Sprintf("Error: No parent process for %d, err message: ", pid, err))
			return "-1", nil
		}

		if 1 == p.Pid() {
			not_init = false
		}
	}

	DebugMessage(fmt.Sprintf("Info: Container shim not found for process for %d", pid))
	cu.pidCache.Set(pid, "0")
	return "0", nil
}
